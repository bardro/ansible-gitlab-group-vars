# run

```sh
REQUESTS_CA_BUNDLE=./ca-chain.pem
source .env
ansible-playbook playbook-groups.yaml -e API_TOKEN=$API_TOKEN
```
vault:
```sh

ansible-vault decrypt inventory/gitlabs/group_vars/gitlab_test/vault.yaml --vault-password-file .vault_password

ansible-vault encrypt inventory/gitlabs/group_vars/gitlab_test/vault.yaml --vault-password-file .vault_password
```
